package main

import (
	"errors"
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"strings"

	"github.com/julienschmidt/httprouter"
	"github.com/nsqio/go-nsq"
	"gitlab.com/lercher/nsqpro/pasing"
)

var (
	flagResponseURL = flag.String("responseURL", "nsqd://ntsdtsd.singhammer.de:4150/$t", "address of self for the Response-Url message header field, where $t is the consumer topic name including #ephemeral, $h is the hostname, for http use e.g. http://ntsdtsd.singhammer.de:4151/pub?topic=$t")
	flagPublisher   = flag.String("publisher", "nsqd://ntsdtsd.singhammer.de:4150", "an nsqd:// moniker or an http/https URL, where a message is posted to, e.g. http://ntsdtsd.singhammer.de:4151/pub?topic=$t, where $t is replaced by the topic name")
	flagConsumer    = flag.String("consumer", "nsqd://ntsdtsd.singhammer.de:4150", "an nsqd://host:4150 or lookupd://host:4160 moniker")
	flagTopic       = flag.String("topic", "OBK", "topic to publish messages to, should identify the pro database")
	flagTenantid    = flag.String("tenant", "OBK001", "(transient property: this value is usually specified in the UI and will go to all pasing messages in the lsmandant attribute)")
	flagChannel     = flag.String("channel", "nsqpro#ephemeral", "this process' consumer channel name")
)

var (
	// tenant can be a proper pro ID like OBK001
	// but also *OBK001 which declares the so called Alle-Mandant for queries and with create-record-in=OBK001
	tenantTODO    pasing.TenantID
	topic         Topic
	consumertopic Topic
	pub           publisher
	fa            = &flightattendant{}
	n             int
)

type route string

const (
	routeLSPRO     = route("lspro")
	routeLSSERVICE = route("lsservice")
)

func main() {
	log.Println("This is nsqpro, (C) 2020 by Martin Lercher")
	flag.Parse()

	publisher, err := makePublisher(*flagPublisher)
	if err != nil {
		log.Fatal(err)
	}

	consumertopic = Topic(fmt.Sprintf("%s-P%d#ephemeral", hostname(), os.Getpid()))
	channel := *flagChannel

	config := nsq.NewConfig()
	consumer, err := nsq.NewConsumer(string(consumertopic), channel, config)
	if err != nil {
		log.Fatal(err)
	}

	fa.attach(consumer, consumertopic, *flagResponseURL)
	log.Printf("Response URL header field will be %q", fa.responseurl)

	consumeraddr := *flagConsumer
	connect := func(addr string) error { return fmt.Errorf("no proper consumer moniker: %s", addr) }
	switch {
	case strings.HasPrefix(consumeraddr, "nsqd://"):
		consumeraddr = strings.TrimPrefix(consumeraddr, "nsqd://")
		connect = consumer.ConnectToNSQD
	case strings.HasPrefix(consumeraddr, "lookupd://"):
		consumeraddr = strings.TrimPrefix(consumeraddr, "lookupd://")
		connect = consumer.ConnectToNSQLookupd
	}
	err = connect(consumeraddr)
	if err != nil {
		log.Fatal(err)
	}

	tenantTODO = pasing.TenantID(*flagTenantid)
	topic = Topic(*flagTopic)
	pub = publisher

	// ^C handler
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	go func() {
		<-c
		log.Println("Shutdown ...")
		publisher.Stop()
		fa.stop()
		log.Println("Shutdown done.")
		os.Exit(0)
	}()

	// http stuff
	router := httprouter.New()
	router.NotFound = http.FileServer(http.Dir("assets"))
	router.GlobalOPTIONS = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if r.Header.Get("Access-Control-Request-Method") != "" {
			// Set CORS headers
			header := w.Header()
			header.Set("Access-Control-Allow-Methods", r.Header.Get("Allow"))
			header.Set("Access-Control-Allow-Origin", "*")
		}

		// Adjust status code to 204
		w.WriteHeader(http.StatusNoContent)
	})

	// API routes
	router.GET("/api/querydatabaseinfo/", apiquerydatabaseinfo)
	router.GET("/api/queryui/", apiqueryui)
	router.GET("/api/queryschemainfo/:type", apiqueryschemainfo)
	router.GET("/api/queryitem/:type", apiqueryitem)
	router.GET("/api/querylist/:key", apiquerylist)
	router.GET("/api/querymru/:key", apiquerymru)
	router.GET("/api/icon/:pixelwidth/:name", apiicon)
	router.POST("/api/logon", apilogon) // User/PW logon
	router.GET("/api/sso/", apisso) // WinAD Single Sign On
	// UI routes
	router.GET("/ui/item/", uiitem)
	router.GET("/ui/listview/", uilistview)

	// setup done, start the http server
	log.Println("starting web server on http://localhost:9000 for topic", topic, ", press ^C to shut down...")
	log.Fatal(http.ListenAndServe(":9000", router))
}

/*
func main() {
	log.Println("This is nsqpro, (C) 2020 by Martin Lercher")
	flag.Parse()

	publisher, err := makePublisher(*flagPublisher)
	if err != nil {
		log.Fatal(err)
	}

	consumertopic := fmt.Sprintf("%s-P%d#ephemeral", hostname(), os.Getpid())
	channel := *flagChannel

	config := nsq.NewConfig()
	consumer, err := nsq.NewConsumer(consumertopic, channel, config)
	if err != nil {
		log.Fatal(err)
	}

	fa := &flightattendant{}
	fa.attach(consumer, consumertopic, *flagResponseURL)
	log.Printf("Response URL header field will be %q", fa.responseurl)

	consumeraddr := *flagConsumer
	connect := func(addr string) error { return fmt.Errorf("no proper consumer moniker: %s", addr) }
	switch {
	case strings.HasPrefix(consumeraddr, "nsqd://"):
		consumeraddr = strings.TrimPrefix(consumeraddr, "nsqd://")
		connect = consumer.ConnectToNSQD
	case strings.HasPrefix(consumeraddr, "lookupd://"):
		consumeraddr = strings.TrimPrefix(consumeraddr, "lookupd://")
		connect = consumer.ConnectToNSQLookupd
	}
	err = connect(consumeraddr)
	if err != nil {
		log.Fatal(err)
	}

	topic := *flagTopic
	n := 0
	log.Println("Enter message, # for sample, end with empty text:")
	scanner := bufio.NewScanner(os.Stdin)
	for scanner.Scan() {
		n++
		txt := scanner.Text()
		if txt == "" {
			break
		}
		var msg string
		if txt == "#" {
			msg = sample()
		} else {
			msg = fmt.Sprintf("<Message lsmandant=%q><Error>#%d: %s</Error></Message>\n", topic, n, txt)
		}

		req := newRR(strconv.Itoa(n), []byte(msg))
		req.Request.Header.Add("Source-Machine", hostname())
		ctx, cancel := context.WithTimeout(context.Background(), 30 * time.Second)
		_, err := fa.publish(ctx, req, publisher, topic, func(err error, inf *inflight) error {
			defer cancel()
			log.Println()
			if err != nil {
				log.Println(inf.CorrelationID, consumertopic, err)
				log.Println()
				return nil
			}
			inf.Response.write(os.Stdout, pasing.XMLPretty)
			log.Println(n, "after", inf.Received.Sub(inf.Sent), "received from", consumertopic)
			log.Println()
			return nil
		})
		if err != nil {
			log.Println("ERROR:", err)
			continue
		}

		log.Println(n, consumertopic, "Sent:")
		req.Request.write(os.Stdout, nil)
		log.Println()
		log.Println("Enter message, # for sample, end with empty text:")
	}
	log.Println("Shutdown ...")
	publisher.Stop()
	fa.stop()
	log.Println("Shutdown done.")
}

func sample() string {
	qdi := &pasing.QueryDatabaseInfo{}
	qdi.Identify = "Query database info"

	qsi := &pasing.QuerySchemaInfo{}
	qsi.Identify = "Query schema info"
	qsi.Typ = "BO"
	qsi.Key = "MAMandantEinstellungen"

	qui := &pasing.QueryUI{}
	qui.Identify = "Query UI"
	qui.Path = "/"

	qi := &pasing.QueryItem{}
	qi.Identify = "Query Item"
	qi.JSONorXML = "JSON"
	qi.Key = "MAMandantEinstellungen"
	qi.ID = "BTV004"

	r := &pasing.Root{}
	r.Mandant = "BTV004"
	r.Add(qdi)
	r.Add(qsi)
	r.Add(qui)
	r.Add(qi)

	x, _ := r.XML()
	return string(x)
}
*/

func makePublisher(addr string) (publisher, error) {
	if strings.HasPrefix(addr, "nsqd://") {
		addr = strings.TrimPrefix(addr, "nsqd://")
		config := nsq.NewConfig()
		return nsq.NewProducer(addr, config)
	}
	return nil, errors.New("this publisher not yet implemented")
}

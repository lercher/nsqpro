package main

import (
	"bufio"
	"io"
	"io/ioutil"
	"net/http"
	"net/textproto"
)

const (
	hdrCorrelationID = "Correlation-Id"
	hdrResponseURL   = "Response-Url"
)

// msg is a binary message with metadata
type msg struct {
	Header http.Header
	Body   []byte
}

func newMsg(body []byte) *msg {
	return &msg{
		Header: make(http.Header),
		Body:   body,
	}
}

func readMsg(rd *bufio.Reader) (*msg, error) {
	r := textproto.NewReader(rd)
	hdr, err := r.ReadMIMEHeader()
	if err != nil {
		return nil, err
	}
	b, err := ioutil.ReadAll(r.R)
	if err != nil {
		return nil, err
	}
	m := &msg{
		Header: http.Header(hdr),
		Body:   b,
	}
	return m, nil
}

func (m *msg) write(w io.Writer, beautifybody func([]byte) ([]byte, error)) error {
	if m == nil {
		return nil
	}
	err := m.Header.Write(w)
	if err != nil {
		return err
	}
	_, err = w.Write([]byte{13, 10}) // CR LF
	if err != nil {
		return err
	}

	b := m.Body
	if beautifybody != nil {
		if newbody, err := beautifybody(m.Body); err == nil {
			b = newbody
		} // else err ignored, just as no beautification happened
	}

	_, err = w.Write(b)
	return err
}

func (m *msg) CorrelationID() string {
	if m == nil {
		return ""
	}
	return m.Header.Get(hdrCorrelationID)
}

package main

import (
	"crypto/md5"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"
	"strings"
	"sync"
	"time"

	"github.com/julienschmidt/httprouter"
	"gitlab.com/lercher/nsqpro/pasing"
)

const minLogonTime = 333 * time.Millisecond

type responseUserData struct {
	Users []responseUser `json:"Data,omitempty"`
}

type responseUser struct {
	IDUSUser   string `json:"idUSUser,omitempty"`
	IDUSGruppe string `json:"idUSGruppe,omitempty"`
	LoginName  string `json:"LoginName,omitempty"`
	KurzName   string `json:"KurzName,omitempty"`
	Password   string `json:"Password,omitempty"`
	USAktiv    bool   `json:"USAktiv,omitempty"`
}

func (u *responseUser) ValidPassword(pw string) bool {
	// TODO: validate pw by MD5 hash
	if !strings.HasPrefix(u.Password, ":") {
		log.Println("LOGON: not an md5 hash in DB for user", u.LoginName)
		return false
	}
	salted := u.IDUSUser + ":" + pw
	hash := md5.Sum([]byte(salted))
	h := fmt.Sprintf(":%X", hash)
	log.Println("LOGON: user", u.LoginName, "-- entered", h, "-- stored", u.Password)
	return h == u.Password
}

func apilogon(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	const filterT = `[["LoginName", "=", $],"and",["LoginMode", "=", 16]]`
	const fields = `["idUSUser", "idUSGruppe", "LoginName", "KurzName", "Password", "USAktiv"]`

	enforceConstantTime := time.After(minLogonTime) // wait at least 1/3s

	err := r.ParseForm()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	name := strings.TrimSpace(r.FormValue("name"))
	pw := strings.TrimSpace(r.FormValue("pw"))
	redirectTo := strings.TrimSpace(r.FormValue("uri"))
	log.Println("LOGON: check for", name, "for uri", redirectTo)

	q := &pasing.QueryList{}
	q.LSMandant = pasing.TenantUnspecified
	q.IDUSUser = pasing.UserUnspecified
	q.Key = "USUser"
	q.Use = "dbo" // list = use list layer, dbo = use dbo table, lsview/"" - use lsView table
	q.RequireTotalCount = "false"
	jname, _ := json.Marshal(name)
	q.Filter = strings.ReplaceAll(filterT, "$", string(jname))
	q.Fields = fields

	q.Identify = r.URL.Path
	p := pasing.MustXML(q)

	mu := new(sync.Mutex)
	mu.Lock()
	mustPublish(p, routeLSSERVICE, func(err error, inf *inflight) {
		defer func() {
			<-enforceConstantTime // wait at least 1/3s before returning to get constant timing
			mu.Unlock()
		}()

		if err != nil {
			log.Println("LOGON: inflight", err)
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		// {"Data":[{"idUSUser":"E4USU18L8GSFSKOHBTA02G124HR86O","idUSGruppe":"System","LoginName":"Admin","KurzName":"Adm","LoginMode":16,"Password":":8BEA55BAA6063B55E1E8BB5BCD4AA38E","USAktiv":true}],"Count":0}
		var response responseUserData
		err = json.Unmarshal(inf.Response.Body, &response)
		if err != nil {
			log.Println("LOGON: unmarshal", err)
			inf.Response.write(os.Stderr, nil)
			http.Error(w, "unmarshal: "+err.Error(), http.StatusInternalServerError)
			return
		}

		// Check if user is found by name and loginmode 16=USER/PW
		if len(response.Users) == 0 {
			log.Println("LOGON: user", name, "unknown or not using pw mode")
			http.Error(w, "logon failed", http.StatusForbidden)
			return
		}

		if len(response.Users) > 1 {
			log.Println("LOGON: user", name, "not unique,", len(response.Users), "users found")
			http.Error(w, "logon failed", http.StatusForbidden)
			return
		}

		theUser := response.Users[0]
		if !theUser.USAktiv {
			log.Println("LOGON: user", name, "not active")
			http.Error(w, "logon failed", http.StatusForbidden)
			return
		}

		if !theUser.ValidPassword(pw) {
			log.Println("LOGON: user", name, "password check failed")
			http.Error(w, "logon failed", http.StatusForbidden)
			return
		}

		log.Println("LOGON: OK", theUser)
		// TODO: set cookie
		if redirectTo == "" {
			redirectTo = "/"
		}
		http.Redirect(w, r, redirectTo, http.StatusTemporaryRedirect)
	})

	mu.Lock()
}

func apisso(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	http.Error(w, "Not yet implemented", http.StatusNotImplemented)
}

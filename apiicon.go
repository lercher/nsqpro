package main

import (
	"net/http"
	"strconv"
	"strings"

	"github.com/julienschmidt/httprouter"
	"github.com/lercher/identicon"
)

func apiicon(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	name := ps.ByName("name")
	name = strings.TrimSuffix(name, ".png")

	pw, err := strconv.Atoi(ps.ByName("pixelwidth"))
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	w.Header().Add("Cache-Control", "public, max-age=31536000")

	i := identicon.Generate([]byte(name))
	_ = i.WritePNGImage(w, pw, identicon.LightBackground(true))
}

# nsqpro

Adapting a request/response protocol to an async queued pub/sub protocol.
In our case, this will be http communicating with a backend service via nsq.

**Important Note:** This repo is abandoned. It is only kept for historic purposes.

## TODO

- none

## DONE

- change inflight's Ready chan to a continuation function
- move back wg.Done() to notify()
- introduce a context.Context for publish() with cancel and
  timeout. Research, how to time-out sth that is sitting in the
  map [see here](https://stackoverflow.com/questions/19992334/how-to-listen-to-n-channels-dynamic-select-statement).
- keep a canceled map of CorrelationIDs to be more precise on non-awaited messages
- provide an http server on top

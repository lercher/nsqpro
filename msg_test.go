package main

import (
	"bufio"
	"bytes"
	"strings"
	"testing"
)

const SampleMsg = `Correlation-Id: 12345
Sender: me

This is the
rest of
the message
  as bytes`

func Test_readMsg(t *testing.T) {
	r := strings.NewReader(SampleMsg)
	rd := bufio.NewReader(r)
	msg, err := readMsg(rd)
	if err != nil {
		t.Fatal(err)
	}

	sender := msg.Header.Get("Sender")
	if sender != "me" {
		t.Errorf("Want sender me, got %s", sender)
	}

	cid := msg.Header.Get("Correlation-Id")
	if cid != "12345" {
		t.Errorf("Want Correlation-Id 12345, got %s", cid)
	}

	b := string(msg.Body)
	want := "This is the\nrest of\nthe message\n  as bytes"
	if b != want {
		t.Errorf("want body\n%q\ngot\n%q\n", want, b)
	}
}

func Test_write(t *testing.T) {
	wantCID := "this-thing"
	m := newMsg([]byte("This is the text\nand a LF"))
	m.Header.Add(hdrCorrelationID, wantCID)
	m.Header.Add("a-encoding", "utf-8")

	buf := new(bytes.Buffer)
	err := m.write(buf, nil)
	if err != nil {
		t.Fatal(err)
	}
	got := string(buf.Bytes())
	want := "A-Encoding: utf-8\r\nCorrelation-Id: this-thing\r\n\r\nThis is the text\nand a LF"
	if got != want {
		t.Errorf("want %q, got %q", want, got)
	}

	gotCID := m.CorrelationID()
	if gotCID != wantCID {
		t.Errorf("want CorrelationID %q, got %q", wantCID, gotCID)
	}
}

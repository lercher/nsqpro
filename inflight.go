package main

import (
	"context"
	"time"
)

type inflight struct {
	rr
	Sent, Received time.Time
	Topic          Topic
	Ctx            context.Context
	Ready          continuation
}

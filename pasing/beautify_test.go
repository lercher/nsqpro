package pasing

import (
	"testing"
)

func TestXMLPretty(t *testing.T) {
	have := `<a><b/><c/></a>`
	want := `<a>
  <b/>
  <c/>
</a>`
	gotx, err := XMLPretty([]byte(have))
	if err != nil {
		t.Fatal(err)
	}
	got := string(gotx)
	if got != want {
		t.Errorf("got %s, want %s", got, want)
	}
}

package pasing

import "encoding/xml"

// Response forms the general data ls.pro sends as a response
type Response struct {
	Log ResponseLog `xml:"Protokoll"`
}

// ResponseLog forms some general data ls.pro sends as a response
type ResponseLog struct {
	XMLName         xml.Name `xml:"Protokoll"`
	ID              string
	Result          string
	ProcessedAt     string
	Identify        string
	IDAdresse       string `xml:"idAdresse"`
	IDVertrag       string `xml:"idVertrag"`
	IDObjekt        string `xml:"idObjekt"`
	IDTask          string `xml:"idTask"`
	KurzesProtokoll string
	LangesProtokoll string
}

// ResponseFromXML deserializes a
func ResponseFromXML(b []byte, r interface{}) error {
	err := xml.Unmarshal(b, r)
	if err != nil {
		return err
	}

	// test r for an interface to do some customized JSON deserialization, after XML is unmarshaled
	if iu, ok := r.(InnerUnmarshaler); ok {
		err = iu.InnerUnmarshal()
		if err != nil {
			return err
		}
	}

	return nil
}

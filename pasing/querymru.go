package pasing

import "encoding/xml"

// QueryMRU retrieves a list ls.pro MRUs.
// It is implemented by lsservice, so there needs to be a Route: lsservice header line.
type QueryMRU struct {
	XMLName xml.Name `xml:"QueryMRU"`
	Child
	LSMandant     TenantID `xml:"lsmandant"`
	IDUSUser      UserID   `xml:"idususer"`
	Key           string   `xml:"key"`
	IncludeLsAlle bool     `xml:"includeLsAlle"`
}

package pasing

import "testing"

func TestRootXML(t *testing.T) {
	qdi := &QueryDatabaseInfo{}
	qdi.Identify = "Query database info"

	qsi := &QuerySchemaInfo{}
	qsi.Identify = "Query schema info"
	qsi.Typ = "BO"
	qsi.Key = "MAMandant"

	qui := &QueryUI{}
	qui.Identify = "Query UI"
	qui.Path = "/"

	qi := &QueryItem{}
	qi.Identify = "Query Item"
	qi.JSONorXML = "JSON"
	qi.Key = "MAMandant"
	qi.ID = "BTV004"

	r := &Root{}
	r.Mandant = "BTV004"
	r.Add(qdi)
	r.Add(qsi)
	r.Add(qui)
	r.Add(qi)

	want := `<Pasing lsmandant="BTV004">
  <QueryDatabaseInfo identify="Query database info"/>
  <QuerySchemaInfo identify="Query schema info" typ="BO" key="MAMandant"/>
  <QueryUI identify="Query UI">
    <Path>/</Path>
  </QueryUI>
  <QueryItem identify="Query Item" typ="JSON">
    <key>MAMandant</key>
    <id>BTV004</id>
  </QueryItem>
</Pasing>`

	bytes, err := r.XML()
	if err != nil {
		t.Fatal(err)
	}
	got := string(bytes)
	if got != want {
		t.Errorf("want\n%s\n\ngot\n\n%s", want, got)
	}
}

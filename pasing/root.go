package pasing

import (
	"encoding/xml"
	"regexp"
)

// Root is the base container of aln ls.pro communication
type Root struct {
	XMLName  xml.Name `xml:"Pasing"`
	Mandant  TenantID `xml:"lsmandant,attr"`
	Children []interface{}
}

type TenantID string
type UserID string

const TenantUnspecified = TenantID("lsAlle")
const UserUnspecified = UserID("")

var re = regexp.MustCompile(`\>\</[-a-zA-Z0-9]+\>`)

// XML returns the Pasing request as XML
func (r *Root) XML() ([]byte, error) {
	b, err := xml.MarshalIndent(r, "", "  ")
	if err != nil {
		return nil, err
	}
	// <QueryDatabaseInfo identify="Query database info"></QueryDatabaseInfo>
	// <QueryDatabaseInfo identify="Query database info"/>
	b = re.ReplaceAll(b, []byte("/>"))
	return b, nil
}

// Add adds a 1st level child element of a communication to the list
func (r *Root) Add(child interface{}) {
	r.Children = append(r.Children, child)
}

// Must returns the XML bytes of a new Root with a single child, it panics on an XML error
func Must(mandant TenantID, child interface{}) []byte {
	r := &Root{
		Mandant:  mandant,
		Children: []interface{}{child},
	}
	b, err := r.XML()
	if err != nil {
		panic(err)
	}
	return b
}

// MustXML returns the XML bytes of a child element only, it panics on an XML error
func MustXML(child interface{}) []byte {
	b, err := xml.MarshalIndent(child, "", "  ")
	if err != nil {
		panic(err)
	}
	b = re.ReplaceAll(b, []byte("/>"))
	return b
}

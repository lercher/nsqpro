package pasing

import "encoding/xml"

import "bytes"

// adapted from https://groups.google.com/forum/#!topic/golang-nuts/lHPOHD-8qio

type node struct {
	Attr     []xml.Attr
	XMLName  xml.Name
	Children []node `xml:",any"`
	Text     string `xml:",chardata"`
}

// XMLPretty pretty-prints xml
func XMLPretty(x []byte) ([]byte, error) {
	n := node{}
	if err := xml.Unmarshal(x, &n); err != nil {
		return nil, err
	}
	b, err := xml.MarshalIndent(n, "", "  ")
	if err != nil {
		return nil, err
	}
	b = re.ReplaceAll(b, []byte("/>"))
	b = bytes.ReplaceAll(b, []byte(`&#xA;`), []byte("\n"))
	b = bytes.ReplaceAll(b, []byte(`&#34;`), []byte(`"`))
	b = bytes.ReplaceAll(b, []byte(`&#39;`), []byte(`'`))
	return b, nil
}

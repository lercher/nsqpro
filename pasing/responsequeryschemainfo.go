package pasing

import (
	"fmt"
	"encoding/json"
)

// ResponseQuerySchemaInfo is ls.pro's response for a QuerySchemaInfo request
type ResponseQuerySchemaInfo struct {
	Response
	Key                   string      `xml:"key"`
	Singular              string      `xml:"singular"`
	Plural                string      `xml:"plural"`
	AddInKey              string      `xml:"addInKey"`
	IsView                string      `xml:"isView"`
	TableName             string      `xml:"tablename"`
	PKName                string      `xml:"pkName"`
	Database              string      `xml:"database"`
	Timestamp             string      `xml:"timestamp"`
	JSONExclusiveSubtypes []byte      `xml:"exclusiveSubtypes"` // JSON
	JSONTabs              []byte      `xml:"tabs"`              // JSON
	JSONFields            []byte      `xml:"instanzen"`         // JSON
	Fields                interface{} `xml:"-"`
	Tabs                  interface{} `xml:"-"`
	ExclusiveSubtypes     interface{} `xml:"-"`
}

func (rqsi *ResponseQuerySchemaInfo) InnerUnmarshal() (err error) {
	err = json.Unmarshal(rqsi.JSONTabs, &rqsi.Tabs)
	if err != nil {
		return fmt.Errorf("JSON tabs: %v in %s", err, rqsi.JSONTabs)
	}

	err = json.Unmarshal(rqsi.JSONFields, &rqsi.Fields)
	if err != nil {
		return fmt.Errorf("JSON instanzen: %v in %s", err, rqsi.JSONFields)
	}

	err = json.Unmarshal(rqsi.JSONExclusiveSubtypes, &rqsi.ExclusiveSubtypes)
	if err != nil {
		return fmt.Errorf("JSON exclusiveSubtypes: %v in %s", err, rqsi.ExclusiveSubtypes)
	}

	return nil
}

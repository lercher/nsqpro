package pasing

import "encoding/xml"

// https://innersource.soprasteria.com/leasysoft/leasy/blob/master/lsLS/lsAITrans/CBizTalkQSchemaInfo.cls

// QuerySchemaInfo queries for schema information of a particular ls.pro object
type QuerySchemaInfo struct {
	XMLName xml.Name `xml:"QuerySchemaInfo"`
	Child
	Typ string `xml:"typ,attr"`
	Key string `xml:"key,attr"`
}

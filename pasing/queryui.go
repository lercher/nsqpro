package pasing

import "encoding/xml"

// https://innersource.soprasteria.com/leasysoft/leasy/blob/master/lsLS/lsAITrans/CBizTalkQUI.cls

// QueryUI queries ls.pro for how to expand a particular path, starting at "/"
type QueryUI struct {
	XMLName xml.Name `xml:"QueryUI"`
	Child
	Path string `xml:"Path"`
}

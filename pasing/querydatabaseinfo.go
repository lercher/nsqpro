package pasing

import "encoding/xml"

// https://innersource.soprasteria.com/leasysoft/leasy/blob/master/lsLS/lsAITrans/CBizTalkQDI.cls

// QueryDatabaseInfo lists all ls.pro tenants with key and display name
type QueryDatabaseInfo struct {
	XMLName xml.Name `xml:"QueryDatabaseInfo"`
	Child
	// empty
}

package pasing

import "encoding/json"

import "fmt"

// ResponseQueryItem is ls.pro's response for a QueryItem request
type ResponseQueryItem struct {
	Response
	Display               string      `xml:"display"`
	Key                   string      `xml:"key"`
	Singular              string      `xml:"singular"`
	Plural                string      `xml:"plural"`
	AddInKey              string      `xml:"addInKey"`
	IsView                string      `xml:"isView"`
	TableName             string      `xml:"tablename"`
	PKName                string      `xml:"pkName"`
	Database              string      `xml:"database"`
	ID                    string      `xml:"id"`
	ParentID              string      `xml:"parentid"`
	Timestamp             string      `xml:"timestamp"`
	JSONExclusiveSubtypes []byte      `xml:"exclusiveSubtypes"` // JSON
	JSONTabs              []byte      `xml:"tabs"`              // JSON
	JSONFields            []byte      `xml:"instanzen"`         // JSON
	Messages              string      `xml:"messages"`
	Fields                interface{} `xml:"-"`
	Tabs                  interface{} `xml:"-"`
	ExclusiveSubtypes     interface{} `xml:"-"`
}

func (rqi *ResponseQueryItem) InnerUnmarshal() (err error) {
	err = json.Unmarshal(rqi.JSONTabs, &rqi.Tabs)
	if err != nil {
		return fmt.Errorf("JSON tabs: %v in %s", err, rqi.JSONTabs)
	}

	err = json.Unmarshal(rqi.JSONFields, &rqi.Fields)
	if err != nil {
		return fmt.Errorf("JSON instanzen: %v in %s", err, rqi.JSONFields)
	}

	err = json.Unmarshal(rqi.JSONExclusiveSubtypes, &rqi.ExclusiveSubtypes)
	if err != nil {
		return fmt.Errorf("JSON exclusiveSubtypes: %v in %s", err, rqi.ExclusiveSubtypes)
	}

	return nil
}

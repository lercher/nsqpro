package pasing

import "encoding/xml"

// https://innersource.soprasteria.com/leasysoft/leasy/blob/master/lsLS/lsAITrans/CBizTalkQItem.cls

// QueryItem retrieves a particular ls.pro object
type QueryItem struct {
	XMLName xml.Name `xml:"QueryItem"`
	Child
	JSONorXML string `xml:"typ,attr"`
	Key       string `xml:"key"`
	ID        string `xml:"id"`
}

package pasing

import "encoding/xml"

// QueryList retrieves a list of particular ls.pro objects.
// It is implemented by lsservice, so there needs to be a Route: lsservice header line.
type QueryList struct {
	XMLName xml.Name `xml:"QueryList"`
	Child
	LSMandant         TenantID `xml:"lsmandant"`
	IDUSUser          UserID   `xml:"idususer"`
	Key               string   `xml:"key"`
	Use               string   `xml:"use"`
	Skip              string   `xml:"skip"`
	Take              string   `xml:"take"`
	RequireTotalCount string   `xml:"requireTotalCount"`
	RequireGroupCount string   `xml:"requireGroupCount"`
	Sort              string   `xml:"sort"`
	Filter            string   `xml:"filter"`
	TotalSummary      string   `xml:"totalSummary"`
	Group             string   `xml:"group"`
	GroupSummary      string   `xml:"groupSummary"`
	GroupIncludeNull  bool     `xml:"groupIncludeNull"`
	Fields            string   `xml:"fields"`
}

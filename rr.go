package main

// rr encapsulates an nsq request with an async response
type rr struct {
	CorrelationID string
	Request       *msg
	Response      *msg
}

func newRR(correlationID string, body []byte) *rr {
	req := newMsg(body)
	req.Header.Add(hdrCorrelationID, correlationID)
	return &rr{
		CorrelationID: correlationID,
		Request:       req,
		Response:      nil,
	}
}

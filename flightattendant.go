package main

import (
	"bufio"
	"bytes"
	"context"
	"errors"
	"fmt"
	"log"
	"os"
	"reflect"
	"strings"
	"sync"
	"time"

	"github.com/nsqio/go-nsq"
)

type flightattendant struct {
	consumertopic Topic
	consumer      *nsq.Consumer
	responseurl   string
	mu            sync.Mutex
	inflights     map[string]*inflight
	aborted       map[string]bool
	pool          *sync.Pool
	wg            *sync.WaitGroup
	changed       chan bool
}

func (fa *flightattendant) attach(c *nsq.Consumer, consumertopic Topic, responseURLTemplate string) {
	fa.inflights = make(map[string]*inflight)
	fa.aborted = make(map[string]bool)
	fa.consumer = c
	fa.consumer.AddHandler(fa)
	fa.consumertopic = consumertopic
	fa.responseurl = responseURLTemplate
	fa.responseurl = strings.ReplaceAll(fa.responseurl, "$t", string(consumertopic))
	fa.responseurl = strings.ReplaceAll(fa.responseurl, "$h", hostname())
	fa.pool = new(sync.Pool)
	fa.pool.New = func() interface{} { return new(bytes.Buffer) }
	fa.changed = make(chan bool)

	fa.wg = new(sync.WaitGroup)
	fa.wg.Add(1) // 1 for housekeeping plus one for every inflight
	go fa.housekeeping()
}

func (fa *flightattendant) stop() {
	log.Println("flightattendant stop - trigger housekeeping via trigger close")
	close(fa.changed)
	fa.wg.Wait()
	fa.consumer.Stop() // no more incoming messages
}

var errShutdownAbort = errors.New("abort due to shutdown")

func (fa *flightattendant) housekeeping() {
	for {
		fa.mu.Lock()
		cases := collectContextChannels(fa.changed, fa.inflights)
		fa.mu.Unlock()

		// just wait for a change or a done:
		log.Println("flightattendant-housekeeping waiting for", len(cases)-1, "in-flight(s), or a change...")
		chosen, _, recvOK := reflect.Select(cases)
		// log.Println("flightattendant-housekeeping in action, chosen", chosen, "recvOK", recvOK)
		if chosen == 0 && !recvOK {
			break // i.e. fa.changed was closed
		}

		fa.mu.Lock()
		for cid, inf := range fa.inflights {
			// log.Printf("flightattendant-housekeeping inspecting %q", cid)
			select {
			case <-inf.Ctx.Done():
				// log.Printf("flightattendant-housekeeping canceling %q", cid)
				if inf.Ready != nil {
					delete(fa.inflights, cid)
					err := inf.Ready(inf.Ctx.Err(), inf) // should be quick, b/c inside lock
					if err != nil {
						log.Printf("unexpected in Ready handler of %q: %v", inf.CorrelationID, err)
					}
					fa.aborted[inf.CorrelationID] = true
				}
				fa.wg.Done()
			default:
				// not yet done, so nop
			}
			// log.Printf("flightattendant-housekeeping inspection done %q", cid)
		}
		fa.mu.Unlock()
		// log.Printf("flightattendant-housekeeping all inspection done")
	}

	log.Println("flightattendant-housekeeping closed")
	fa.abortAll()
	fa.wg.Done()
}

func collectContextChannels(trigger chan bool, m map[string]*inflight) []reflect.SelectCase {
	list := make([]reflect.SelectCase, 1, 1+len(m))
	list[0] = reflect.SelectCase{Dir: reflect.SelectRecv, Chan: reflect.ValueOf(trigger)}
	for _, inf := range m {
		list = append(list, reflect.SelectCase{Dir: reflect.SelectRecv, Chan: reflect.ValueOf(inf.Ctx.Done())})
	}
	return list
}

func (fa *flightattendant) abortAll() {
	fa.mu.Lock()
	defer fa.mu.Unlock()

	for cid, inf := range fa.inflights {
		log.Printf("aborting inflight for CorreleationID %q due to shutdown", cid)
		if inf.Ready != nil {
			_ = inf.Ready(errShutdownAbort, inf) // ignore shutdown error
		}
		fa.wg.Done()
	}
}

func hostname() string {
	hn, err := os.Hostname()
	if err != nil {
		return "no-hostname"
	}
	return hn
}

func (fa *flightattendant) HandleMessage(msg *nsq.Message) error {
	r := bytes.NewReader(msg.Body)
	rd := bufio.NewReader(r)
	m, err := readMsg(rd)
	if err != nil {
		return err
	}
	return fa.notify(m)
}

func (fa *flightattendant) notify(m *msg) (err error) {
	cid := m.CorrelationID()

	fa.mu.Lock()
	if inf, ok := fa.inflights[cid]; ok {
		delete(fa.inflights, cid)
		fa.changed <- true
		fa.mu.Unlock()
		inf.Response = m
		inf.Received = time.Now()
		if inf.Ready != nil {
			err = inf.Ready(nil, inf)
		}
		fa.wg.Done()
		return err
	}
	if _, ok := fa.aborted[cid]; ok {
		log.Printf("received msg for already aborted request with CID %q, response ignored", cid)
		delete(fa.aborted, cid)
		fa.mu.Unlock()
		return nil
	}
	fa.mu.Unlock()
	log.Printf("unexpected message with correlation id %s", cid)
	return nil
}

// Topic is the name of an NSQ topic (string)
type Topic string

func (fa *flightattendant) publish(ctx context.Context, r *rr, p publisher, topic Topic, cont continuation) (*inflight, error) {
	t0 := time.Now()
	buf := fa.pool.Get().(*bytes.Buffer)
	defer fa.pool.Put(buf)

	r.Request.Header.Add(hdrResponseURL, fa.responseurl)
	cid := r.Request.CorrelationID()
	buf.Reset()
	err := r.Request.write(buf, nil)
	if err != nil {
		return nil, err
	}

	err = p.Publish(string(topic), buf.Bytes())
	if err != nil {
		return nil, err
	}

	inf := &inflight{
		rr:    *r,
		Topic: topic,
		Ready: cont,
		Ctx:   ctx,
		Sent:  t0,
	}

	fa.mu.Lock()
	defer fa.mu.Unlock()
	if _, ok := fa.inflights[cid]; ok {
		return nil, fmt.Errorf("a message with correlation id %q is already in flight", cid)
	}
	if _, ok := fa.aborted[cid]; ok {
		return nil, fmt.Errorf("a message with correlation id %q was already aborted", cid)
	}
	fa.inflights[cid] = inf
	fa.changed <- true

	fa.wg.Add(1)
	return inf, nil
}

type continuation func(error, *inflight) error

type publisher interface {
	Publish(topic string, body []byte) error
	Stop()
}

module gitlab.com/lercher/nsqpro

go 1.12

require (
	github.com/crestonbunch/godata v0.0.0-20170922022651-303b6c35d5bf
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/gorilla/sessions v1.2.0
	github.com/jcmturner/goidentity/v6 v6.0.1
	github.com/jcmturner/gokrb5/v8 v8.1.0
	github.com/julienschmidt/httprouter v1.3.0
	github.com/lercher/identicon v1.0.0
	github.com/llgcode/draw2d v0.0.0-20200110163050-b96d8208fcfc // indirect
	github.com/niemeyer/pretty v0.0.0-20200227124842-a10e7caefd8e // indirect
	github.com/nsqio/go-nsq v1.0.8
	github.com/stretchr/testify v1.5.1 // indirect
	golang.org/x/crypto v0.0.0-20200221231518-2aa609cf4a9d // indirect
	golang.org/x/image v0.0.0-20200119044424-58c23975cae1 // indirect
	golang.org/x/net v0.0.0-20200226121028-0de0cce0169b // indirect
	gopkg.in/check.v1 v1.0.0-20200227125254-8fa46927fb4f // indirect
	gopkg.in/yaml.v2 v2.2.8 // indirect
)

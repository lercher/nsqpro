package main

import (
	"fmt"
	"html/template"
	"io/ioutil"
	"log"
	"net/http"
	"sync"
	"time"

	"github.com/julienschmidt/httprouter"
	"gitlab.com/lercher/nsqpro/pasing"
)

func uiitem(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	q := &pasing.QueryItem{}
	q.JSONorXML = "JSON"
	q.Key = r.URL.Query().Get("key")
	q.ID = r.URL.Query().Get("id")

	q.Identify = r.URL.Path
	p := pasing.Must(tenant(r), q)
	mustPublishAndTemplate(w, p, routeLSPRO, "queryitem", &pasing.ResponseQueryItem{}).Lock()
}

func uilistview(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	q := &pasing.QuerySchemaInfo{}
	q.Typ = "BO"
	q.Key = r.URL.Query().Get("key")

	q.Identify = r.URL.Path
	p := pasing.Must(tenant(r), q)
	mustPublishAndTemplate(w, p, routeLSPRO, "listview", &pasing.ResponseQuerySchemaInfo{}).Lock()
}

// templateData aggregates ambient data (TODO) with an ls.pro Response
type templateData struct {
	Response interface{}
}

func mustPublishAndTemplate(w http.ResponseWriter, b []byte, route route, templatename string, response interface{}) *sync.Mutex {
	mu := new(sync.Mutex)
	mu.Lock()
	mustPublish(b, route, func(err error, inf *inflight) {
		defer mu.Unlock()
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		var success bool
		for kk, vs := range inf.Response.Header {
			k := "X-Lspro-" + kk
			for _, v := range vs {
				w.Header().Add(k, v)
				if kk == "Success" {
					success = (v == "true")
				}
			}
		}
		w.Header().Add("X-Nsq-Processing-Time", fmt.Sprint(inf.Received.Sub(inf.Sent)))
		w.Header().Add("X-Nsq-Template", templatename)
		if !success {
			w.WriteHeader(http.StatusBadRequest)
		}

		// TODO: This call can and should be cached
		t0 := time.Now()
		templates, err := template.New("").Funcs(tfunc).ParseGlob("templates/*")
		if err != nil {
			http.Error(w, fmt.Sprintf("t-parser: %v", err), http.StatusInternalServerError)
			return
		}
		log.Println("template parsing time:", time.Since(t0))

		err = pasing.ResponseFromXML(inf.Response.Body, response)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		td := templateData{
			Response: response,
			// TODO: add ambient data
		}
		go func() {
			t2 := time.Now()
			_ = templates.ExecuteTemplate(ioutil.Discard, templatename, td)
			log.Println("template execution to nil writer time:", time.Since(t2))
		}()
		t1 := time.Now()
		err = templates.ExecuteTemplate(w, templatename, td)
		if err != nil {
			// normally a template error
			fmt.Fprintf(w, "\n\n\n%v\n", err.Error())
			// but it could be a tcp connection send error, so log the error as well
			log.Println(err)
			return
		}
		log.Println("template execution and http sending time:", time.Since(t1))
	})
	return mu
}

package main

import (
	"fmt"
	"html/template"
	"strings"
)

var tfunc = template.FuncMap{

	"isField": func(fn string) bool {
		return !strings.HasSuffix(fn, "$")
	},

	"meta": func(fields map[string]interface{}, fn string) interface{} {
		m := fn + "$"
		return fields[m]
	},

	"dm": func(fields map[string]interface{}, fn string) (interface{}, error) {
		dm := DM{}
		if tv, ok := fields[fn].(map[string]interface{}); ok {
			dm.Text = fmt.Sprint(tv["text"])
			dm.Value = fmt.Sprint(tv["value"])
			dm.M = fields[fn+"$"]
			dm.Key = fn
			return dm, nil
		}
		return nil, fmt.Errorf("field not found in dm: %v", fn)
	},
}

// DM is Data Metadata of a pro field, where Data consists of Key, Text and Value
// It's created with the "dm" template function
type DM struct {
	Key, Text, Value string
	M                interface{}
}

package main

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"os"
	"strconv"
	"sync"
	"time"

	"github.com/julienschmidt/httprouter"
	"gitlab.com/lercher/nsqpro/pasing"
)

func apiquerydatabaseinfo(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	q := &pasing.QueryDatabaseInfo{}

	q.Identify = r.URL.Path
	p := pasing.Must(pasing.TenantID(""), q)
	mustPublishAndRespond(w, p, routeLSPRO).Lock()
}

func apiqueryui(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	q := &pasing.QueryUI{}
	q.Path = r.URL.Query().Get("path")

	q.Identify = r.URL.Path
	p := pasing.Must(tenant(r), q)
	mustPublishAndRespond(w, p, routeLSPRO).Lock()
}

func apiqueryschemainfo(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	q := &pasing.QuerySchemaInfo{}
	q.Typ = ps.ByName("type")
	q.Key = r.URL.Query().Get("key")

	q.Identify = r.URL.Path
	p := pasing.Must(tenant(r), q)
	mustPublishAndRespond(w, p, routeLSPRO).Lock()
}

func apiqueryitem(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	q := &pasing.QueryItem{}
	q.JSONorXML = ps.ByName("type")
	q.Key = r.URL.Query().Get("key")
	q.ID = r.URL.Query().Get("id")

	q.Identify = r.URL.Path
	p := pasing.Must(tenant(r), q)
	mustPublishAndRespond(w, p, routeLSPRO).Lock()
}

func apiquerylist(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	q := &pasing.QueryList{}
	q.LSMandant = tenant(r)
	q.IDUSUser = idususer(r)
	q.Key = ps.ByName("key")
	q.Use = "list" // list = use list layer, dbo = use dbo table, lsview/"" - use lsView table
	q.Skip = r.URL.Query().Get("skip")
	q.Take = r.URL.Query().Get("take")
	q.RequireTotalCount = r.URL.Query().Get("requireTotalCount")
	q.RequireGroupCount = r.URL.Query().Get("requireGroupCount")
	q.Sort = r.URL.Query().Get("sort")
	q.Filter = r.URL.Query().Get("filter")
	q.TotalSummary = r.URL.Query().Get("totalSummary")
	q.Group = r.URL.Query().Get("group")
	q.GroupSummary = r.URL.Query().Get("groupSummary")
	q.Fields = r.URL.Query().Get("fields")
	q.GroupIncludeNull = true

	q.Identify = r.URL.Path
	p := pasing.MustXML(q)
	mustPublishAndRespond(w, p, routeLSSERVICE).Lock()
}

func apiquerymru(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	q := &pasing.QueryMRU{}
	q.LSMandant = tenant(r)
	q.IDUSUser = idususer(r)
	q.Key = ps.ByName("key")
	q.IncludeLsAlle = false

	q.Identify = r.URL.Path
	p := pasing.MustXML(q)
	mustPublishAndRespond(w, p, routeLSSERVICE).Lock()
}

func tenant(r *http.Request) pasing.TenantID {
	return pasing.TenantID(byCookie(r, "tenant"))
}

func idususer(r *http.Request) pasing.UserID {
	return pasing.UserID(byCookie(r, "idususer"))
}

func byCookie(r *http.Request, what string) string {
	// TODO: do as the name says
	switch what {
	case "tenant":
		return string(tenantTODO)
	case "idususer":
		return "5QUSU15PNHPPFO21CDAFEMJFP8D27V" // TODO
	default:
		return ""
	}
}

func mustPublish(b []byte, route route, respond func(err error, inf *inflight)) {
	n++
	n := n

	req := newRR(strconv.Itoa(n), b)
	req.Request.Header.Add("Source-Machine", hostname())
	req.Request.Header.Add("Route", string(route))
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	if dl, ok := ctx.Deadline(); ok {
		req.Request.Header.Add("Deadline", dl.UTC().Format(time.RFC3339)) // "2006-01-02T15:04:05Z07:00"
	}
	_, err := fa.publish(ctx, req, pub, topic, func(err error, inf *inflight) error {
		defer cancel()
		respond(err, inf)
		log.Println()
		if err != nil {
			log.Println(inf.CorrelationID, consumertopic, err)
			log.Println()
			return nil
		}
		// inf.Response.write(os.Stdout, pasing.XMLPretty)
		log.Println(n, "after", inf.Received.Sub(inf.Sent), "received from", consumertopic)
		log.Println()
		return nil
	})
	if err != nil {
		log.Println("ERROR in publish:", err)
		respond(err, nil)
		return
	}

	log.Println(n, consumertopic, "Sent:")
	req.Request.write(os.Stdout, nil)
	log.Println()
}

func mustPublishAndRespond(w http.ResponseWriter, b []byte, route route) *sync.Mutex {
	mu := new(sync.Mutex)
	mu.Lock()
	mustPublish(b, route, func(err error, inf *inflight) {
		defer mu.Unlock()
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		var success bool
		for kk, vs := range inf.Response.Header {
			k := "X-Lspro-" + kk
			for _, v := range vs {
				w.Header().Add(k, v)
				if kk == "Success" {
					success = (v == "true")
				}
			}
		}
		w.Header().Add("X-Nsq-Processing-Time", fmt.Sprint(inf.Received.Sub(inf.Sent)))
		if route == routeLSPRO {
			w.Header().Add("Content-Type", "text/xml; charset=utf-8")
		}
		if !success {
			w.WriteHeader(http.StatusBadRequest)
		}
		_, err = w.Write(inf.Response.Body)
		if err != nil {
			log.Println("error responding to http:", err)
			return
		}
	})
	return mu
}

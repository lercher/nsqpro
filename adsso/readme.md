# AD SSO

To get the `singhammer_adsso.keytab` file needed, create a user named `adsso`
and then run this script with admin rights.

Important: don't use an existing account, as it will reset its password.

````bat
@echo Need to run this in an admin cmd!
@echo ---------------------------------
@pause
rem setspn -D HTTP/ntsdtsr.singhammer.de adsso
ktpass -princ HTTP/ntsdtsr.singhammer.de@SINGHAMMER.DE -mapuser adsso@SINGHAMMER.DE -pass * -ptype KRB5_NT_PRINCIPAL -crypto All -out singhammer_adsso.keytab
dir *.keytab /l/b
setspn -l adsso
````

````bat
ktpass -princ HTTP/tumble.singhammer.de@SINGHAMMER.DE -mapuser tumble@SINGHAMMER.DE -pass * -ptype KRB5_NT_PRINCIPAL -crypto All -out singhammer_tumble.keytab
````

![In Action](InAction.png)

## krb5.conf

[Link to docs.microsoft.com](https://docs.microsoft.com/de-de/sql/azure-data-studio/enable-kerberos?view=sql-server-ver15)

In AD domain execute:

````bat
nltest /dsgetdc:DOMAIN.COMPANY.COM
````

and note the name after `DC: \\`, e.g. `DC: \\dc-33.domain.company.com`.

Then create a file called `krb5.conf` with this contents:

````txt
[libdefaults]
  default_realm = DOMAIN.COMPANY.COM
 
[realms]
DOMAIN.COMPANY.COM = {
   kdc = dc-33.domain.company.com
}
````

## e.g.

````txt
C:\Users\lercher>nltest /dsgetdc:singhammer.de
           DC: \\NTSDTS0.singhammer.de
      Address: \\192.168.1.30
     Dom Guid: 0000000-ffff-0000-ffff-000000000000
     Dom Name: singhammer.de
  Forest Name: singhammer.de
        Flags: PDC GC DS LDAP KDC TIMESERV GTIMESERV WRITABLE DNS_DC DNS_DOMAIN DNS_FOREST CLOSE_SITE FULL_SECRET WS DS_8 DS_9
The command completed successfully

C:\Users\lercher>
````

````txt
[libdefaults]
  default_realm = SINGHAMMER.DE
 
[realms]
SINGHAMMER.DE = {
   kdc = ntsdts0.singhammer.de
}
````

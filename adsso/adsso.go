package main

import (
	"fmt"
	"log"
	"net/http"
	"os"

	"github.com/jcmturner/goidentity/v6"
	"github.com/jcmturner/gokrb5/v8/credentials"
	"github.com/jcmturner/gokrb5/v8/keytab"
	"github.com/jcmturner/gokrb5/v8/service"
	"github.com/jcmturner/gokrb5/v8/spnego"
	"github.com/jcmturner/gokrb5/v8/config"
)

const (
	port = ":8090"
)

const singhammerConf = `[libdefaults]
default_realm = SINGHAMMER.DE

[realms]
SINGHAMMER.DE = {
 kdc = ntsdts0.singhammer.de
}
`

func main() {
	//defer profile.Start(profile.TraceProfile).Stop()
	// Create logger
	l := log.New(os.Stderr, "GOKRB5 Service: ", log.Ldate|log.Ltime|log.Lshortfile)

	cfg, err := config.NewFromString(singhammerConf)
	if err != nil {
		log.Fatalln(err)
	}
	log.Println("config:", cfg)

	// Load the service's keytab
	kt, err := keytab.Load("singhammer_adsso.keytab")
	if err != nil {
		log.Fatalln(err)
	}
	server := fmt.Sprintf("%s/%s", kt.Entries[0].Principal.Components[0], kt.Entries[0].Principal.Components[1])
	serverPrinc := service.KeytabPrincipal(server)
	log.Println("principal for server, as taken from keytab file:", server)

	// Create the application's specific handler
	th := http.HandlerFunc(testAppHandler)

	// Set up handler mappings wrapping in the SPNEGOKRB5Authenticate handler wrapper
	mux := http.NewServeMux()
	mux.Handle("/", spnego.SPNEGOKRB5Authenticate(th, kt, service.Logger(l), serverPrinc)) // , service.SessionManager(NewSessionMgr("gokrb5"))))

	// Start up the web server
	log.Println("listening on http://localhost" + port + "/")
	log.Fatal(http.ListenAndServe(port, mux))
}

// Simple application specific handler
func testAppHandler(w http.ResponseWriter, r *http.Request) {
	creds := goidentity.FromHTTPRequestContext(r)
	fmt.Fprint(w, "<html>\n<p><h1>TEST.GOKRB5 Handler</h1></p>\n")
	if creds != nil && creds.Authenticated() {
		fmt.Fprintf(w, "<ul><li>Authenticed user: %s</li>\n", creds.UserName())
		fmt.Fprintf(w, "<li>User's realm: %s</li>\n", creds.Domain())
		fmt.Fprint(w, "<li>Authz Attributes (Group Memberships):</li><ul>\n")
		for _, s := range creds.AuthzAttributes() {
			fmt.Fprintf(w, "<li>%v</li>\n", s)
		}
		fmt.Fprint(w, "</ul>\n")
		if ADCredsUntyped, ok := creds.Attributes()[credentials.AttributeKeyADCredentials]; ok {
			// log.Printf("%T", ADCredsUntyped) -> credentials.ADCredentials
			if ADCreds, ok := ADCredsUntyped.(credentials.ADCredentials); ok {
				// Now access the fields of the ADCredentials struct. For example:
				fmt.Fprintf(w, "<li>EffectiveName: %v</li>\n", ADCreds.EffectiveName)
				fmt.Fprintf(w, "<li>FullName: %v</li>\n", ADCreds.FullName)
				fmt.Fprintf(w, "<li>UserID: %v</li>\n", ADCreds.UserID)
				fmt.Fprintf(w, "<li>PrimaryGroupID: %v</li>\n", ADCreds.PrimaryGroupID)
				fmt.Fprintf(w, "<li>Group SIDs: %v</li>\n", ADCreds.GroupMembershipSIDs)
				fmt.Fprintf(w, "<li>LogOnTime: %v</li>\n", ADCreds.LogOnTime)
				fmt.Fprintf(w, "<li>LogOffTime: %v</li>\n", ADCreds.LogOffTime)
				fmt.Fprintf(w, "<li>PasswordLastSet: %v</li>\n", ADCreds.PasswordLastSet)
				fmt.Fprintf(w, "<li>LogonServer: %v</li>\n", ADCreds.LogonServer)
				fmt.Fprintf(w, "<li>LogonDomainName: %v</li>\n", ADCreds.LogonDomainName)
				fmt.Fprintf(w, "<li>LogonDomainID: %v</li>\n", ADCreds.LogonDomainID)
			}
		}
		fmt.Fprint(w, "</ul>")
	} else {
		w.WriteHeader(http.StatusUnauthorized)
		fmt.Fprint(w, "Authentication failed")
	}
	fmt.Fprint(w, "</html>")
	return
}

/*

// GOKRB5 Service: 2020/02/11 22:58:50 spnego.go:95: SPNEGO could not create new session: securecookie: the value is too long

type SessionMgr struct {
	skey       []byte
	store      sessions.Store
	cookieName string
}

func NewSessionMgr(cookieName string) SessionMgr {
	skey := []byte("thisistestsecret") // Best practice is to load this key from a secure location.
	return SessionMgr{
		skey:       skey,
		store:      sessions.NewCookieStore(skey),
		cookieName: cookieName,
	}
}

func (smgr SessionMgr) Get(r *http.Request, k string) ([]byte, error) {
	s, err := smgr.store.Get(r, smgr.cookieName)
	if err != nil {
		return nil, err
	}
	if s == nil {
		return nil, errors.New("nil session")
	}
	b, ok := s.Values[k].([]byte)
	if !ok {
		return nil, fmt.Errorf("could not get bytes held in session at %s", k)
	}
	return b, nil
}

func (smgr SessionMgr) New(w http.ResponseWriter, r *http.Request, k string, v []byte) error {
	s, err := smgr.store.New(r, smgr.cookieName)
	if err != nil {
		return fmt.Errorf("could not get new session from session manager: %v", err)
	}
	s.Values[k] = v
	return s.Save(r, w)
}
*/

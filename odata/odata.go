package main

import (
	"log"
	"net/url"

	"github.com/crestonbunch/godata"
)

const od0 = `Products?$select=ID%2CDescription&$filter=ID%20gt%201&$orderby=ID%20desc&$top=1&$count=true&$search=tom`
const od1 = `Products?%24orderby=Product_Cost%20desc&%24top=20&%24select=Product_ID%2CProduct_Name%2CProduct_Cost%2CProduct_Sale_Price%2CProduct_Retail_Price%2CProduct_Current_Inventory&%24filter=Product_Current_Inventory%20gt%2020&%24inlinecount=allpages`
const od2 = `Products?%24orderby=Product_Cost%20desc&%24top=20&%24select=Product_ID%2CProduct_Name%2CProduct_Cost%2CProduct_Sale_Price%2CProduct_Retail_Price%2CProduct_Current_Inventory&%24filter=(Product_Current_Inventory%20gt%2020)%20and%20(Product_Current_Inventory%20eq%2020)&%24inlinecount=allpages&$apply=groupby((status),aggregate(id+with+countdistinct+as+total))`
// https://js.devexpress.com/Demos/WidgetsGallery/Demo/DataGrid/ODataService/jQuery/Light/
// $apply=groupby((status),aggregate(id with countdistinct as total)) -> https://stackoverflow.com/questions/41550860/odata-v4-groupby-with-count

func main() {
	od := od2
	log.Println("Parsing", od)

	u, err := url.Parse(od)
	if err != nil {
		log.Fatalln(err)
	}

	log.Println(u.Query())

	request, err := godata.ParseRequest(u.Path, u.Query())
	if err != nil {
		log.Fatalln(err)
	}

	log.Println("FirstSegment.Name", request.FirstSegment.Name)
	
	for i, si := range request.Query.Select.SelectItems {
		for _, t := range si.Segments {
			log.Println("Query.Select.SelectItem", i, t.Value)
		}
	}
	
	// Note: request.Query.OrderBy is nil if clause is missing
	for i, oi := range request.Query.OrderBy.OrderByItems {
		log.Println("Query.OrderBy.OrderByItems", i, oi.Field.Value, oi.Order)
	}
	log.Println("Query.Filter.Tree:")
	printTree(request.Query.Filter.Tree, 0)
	log.Println("End Filter")

	log.Println("Query.Apply", *request.Query.Apply)

	log.Println("Query.Top", *request.Query.Top)
}

func printTree(n *godata.ParseNode, level int) {
	indent := ""
	for i := 0; i < level; i++ {
		indent += "  "
	}
	log.Printf("%s %s\n", indent, n.Token.Value)
	for _, v := range n.Children {
		printTree(v, level+1)
	}
}

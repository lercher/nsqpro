# OData

## With Parser for Go

Parsing such a query:

````txt
$apply:        [groupby((status),aggregate(id with countdistinct as total))]
$filter:       [(Product_Current_Inventory gt 20) and (Product_Current_Inventory eq 20)]
$inlinecount:  [allpages]
$orderby:      [Product_Cost desc]
$select:       [Product_ID,Product_Name,Product_Cost,Product_Sale_Price,Product_Retail_Price,Product_Current_Inventory]
$top:          [20]
````

yields

````txt
2020/02/13 18:21:02 map[$apply:[groupby((status),aggregate(id with countdistinct as total))] $filter:[(Product_Current_Inventory gt 20) and (Product_Current_Inventory eq 20)] $inlinecount:[allpages] $orderby:[Product_Cost desc] $select:[Product_ID,Product_Name,Product_Cost,Product_Sale_Price,Product_Retail_Price,Product_Current_Inventory] $top:[20]]
2020/02/13 18:21:02 FirstSegment.Name Products
2020/02/13 18:21:02 Query.Select.SelectItem 0 Product_ID
2020/02/13 18:21:02 Query.Select.SelectItem 1 Product_Name
2020/02/13 18:21:02 Query.Select.SelectItem 2 Product_Cost
2020/02/13 18:21:02 Query.Select.SelectItem 3 Product_Sale_Price
2020/02/13 18:21:02 Query.Select.SelectItem 4 Product_Retail_Price
2020/02/13 18:21:02 Query.Select.SelectItem 5 Product_Current_Inventory
2020/02/13 18:21:02 Query.OrderBy.OrderByItems 0 Product_Cost desc
2020/02/13 18:21:02 Query.Filter.Tree:
2020/02/13 18:21:02  and
2020/02/13 18:21:02    gt
2020/02/13 18:21:02      Product_Current_Inventory
2020/02/13 18:21:02      20
2020/02/13 18:21:02    eq
2020/02/13 18:21:02      Product_Current_Inventory
2020/02/13 18:21:02      20
2020/02/13 18:21:02 End Filter
2020/02/13 18:21:02 Query.Apply groupby((status),aggregate(id with countdistinct as total))
2020/02/13 18:21:02 Query.Top 20
````

## For VB An EDM Model is needed

````VB
<Conditional("DEBUG")>
    Private Sub ODataparser()
        ' see https://docs.microsoft.com/en-us/odata/odatalib/use-uri-parser#parsing-query-options
        Dim u = New Uri("Products?$select=ID&$filter=ID%20gt%201&$orderby=ID%20desc&$top=1&$count=true&$search=tom", UriKind.Relative)
        Console.WriteLine(u.ToString())
        ' see https://docs.microsoft.com/en-us/odata/odatalib/edm/build-basic-model
        Dim model = NewEdmModel()

        Try
            Dim parser = New Microsoft.OData.UriParser.ODataUriParser(model, u)
            Dim expand = parser.ParseSelectAndExpand() ' parse $Select, $expand
            Dim filter = parser.ParseFilter()                ' parse $filter
            Dim orderby = parser.ParseOrderBy()             ' parse $orderby
            Dim search = parser.ParseSearch()                ' parse $search
            Dim top = parser.ParseTop()                             ' parse $top
            Dim skip = parser.ParseSkip()                           ' parse $skip
            Dim count = parser.ParseCount()                         ' parse $count
            Console.WriteLine("expand  {0}", expand.AllSelected)
            For Each s In expand.SelectedItems
                Console.WriteLine("select     {0}", s)
            Next
            Console.WriteLine("filter  {0}", filter)
            Console.WriteLine("orderby {0}", orderby)
            Console.WriteLine("search  {0}", search)
            Console.WriteLine("top     {0}", top)
            Console.WriteLine("skip    {0}", skip)
            Console.WriteLine("count   {0}", count)
            Console.WriteLine("----------------------------------")
        Catch ex As Exception
            Console.WriteLine(ex.Message)
        End Try
    End Sub

    Private Function NewEdmModel() As Microsoft.OData.Edm.EdmModel
        Dim model = New Microsoft.OData.Edm.EdmModel

        Dim product = New Microsoft.OData.Edm.EdmEntityType("NS", "Product")
        With product
            Dim PK = .AddStructuralProperty("ID", Microsoft.OData.Edm.EdmPrimitiveTypeKind.Int64)
            .AddKeys(PK)
            .AddStructuralProperty("Name", Microsoft.OData.Edm.EdmPrimitiveTypeKind.String)
            .AddStructuralProperty("Description", Microsoft.OData.Edm.EdmPrimitiveTypeKind.String)
        End With
        model.AddElement(product)

        ' Must have exactly one container:
        Dim container = New Microsoft.OData.Edm.EdmEntityContainer("NS", "DefaultContainer")
        container.AddEntitySet("Products", product)
        model.AddElement(container)

        Return model
    End Function
````

plus these Nuget Packages for Framework 4.5:

````xml
<package id="Microsoft.OData.Core" version="7.6.3" targetFramework="net45" />
<package id="Microsoft.OData.Edm" version="7.6.3" targetFramework="net45" />
<package id="Microsoft.Spatial" version="7.6.3" targetFramework="net45" />
````
